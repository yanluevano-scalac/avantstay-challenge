import cats.effect.std.Supervisor
import cats.effect.{ExitCode, IO, IOApp}
import com.comcast.ip4s.IpLiteralSyntax
import io.scalac.config.HttpApi
import io.scalac.config.data.HttpServerConfig
import io.scalac.db.DataSource
import io.scalac.db.repository.HeadlinesRepository
import io.scalac.http.ControllerValidation
import io.scalac.modules.Services
import org.typelevel.log4cats.slf4j.Slf4jLogger

object Boot extends IOApp {
  implicit val logger = Slf4jLogger.getLogger[IO]

  def run(args: List[String]): IO[ExitCode] = {
    Supervisor[IO]
      .map { implicit sp =>
        val datasource = DataSource.make()
        val httpServerConfig = HttpServerConfig(host"localhost", port"8084")
        val validations = ControllerValidation.make[IO]()
        val headLineRepo = HeadlinesRepository.make[IO](datasource)
        val services    = Services.make[IO](headLineRepo)
        val api         = HttpApi.make[IO](services, validations)
        (httpServerConfig, api.httpApp)
      }
      .flatMap {
        case (cfg, httpApp) =>
          HttpServer[IO].newEmber(cfg, httpApp)
      }
      .useForever
  }




}
