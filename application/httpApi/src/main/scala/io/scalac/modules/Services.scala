package io.scalac.modules

import cats.effect.Async
import io.scalac.db.repository.HeadlinesRepository
import io.scalac.services.{CrawlerService, GraphQLService}

object Services {

  def make[F[_]: Async](repo: HeadlinesRepository[F]): Services[F] = {
    new Services[F](
      crawlerService = CrawlerService.make[F](repo),
      graphQLService = GraphQLService.make[F](repo)
    ) {}
  }

}
//sealed abstract so that implementations are only defined in this file
sealed abstract class Services[F[_]] private (
    val crawlerService: CrawlerService[F],
    val graphQLService: GraphQLService[F]
)
