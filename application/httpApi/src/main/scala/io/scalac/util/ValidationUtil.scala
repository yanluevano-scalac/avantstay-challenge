package io.scalac.util

import cats.implicits._
import cats.data.ValidatedNec

object ValidationUtil {
  type ValidationResult[A] = ValidatedNec[String, A]

  def positiveInt(str: String): ValidationResult[Int] =
    if (str.matches("\\d+") && str.toInt > 0)
      str.toInt.validNec
    else s"$str is not a valid positive int".invalidNec

  def notBlank(name: String, str: String): ValidationResult[String] =
    if (str != null && !str.isEmpty)
      str.validNec
    else s"s$name cant be blank".invalidNec
}
