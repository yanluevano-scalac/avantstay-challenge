package io.scalac.config

import cats.{Applicative, Monad, MonadError, MonadThrow}
import cats.effect.Async
import cats.effect.kernel.MonadCancel
import cats.implicits.toSemigroupKOps
import io.scalac.http.ControllerValidation
import io.scalac.http.routes.{CrawlerRoutes, GraphQlRoutes}
import io.scalac.modules.Services
import org.http4s.implicits.http4sKleisliResponseSyntaxOptionT
import org.http4s.{HttpApp, HttpRoutes}
import org.http4s.server.Router
import org.http4s.server.middleware.{AutoSlash, CORS, RequestLogger, ResponseLogger, Timeout}

import scala.concurrent.duration.DurationInt

object HttpApi {
  def make[F[_]: Async](services: Services[F], validations: ControllerValidation[F]): HttpApi[F] =
    new HttpApi[F](services, validations) {}
}

sealed abstract class HttpApi[F[_]: Async] private(
    services: Services[F],
    validations: ControllerValidation[F]
) {

  private val crawlerRoutes = new CrawlerRoutes[F](services.crawlerService).routes
  private val graphQlRoutes = new GraphQlRoutes[F](services.graphQLService).routes

  private val openRoutes: HttpRoutes[F] =
    crawlerRoutes <+> graphQlRoutes

  private val loggers: HttpApp[F] => HttpApp[F] = {
    { http: HttpApp[F] =>
      RequestLogger.httpApp(true, true)(http)
    } andThen { http: HttpApp[F] =>
      ResponseLogger.httpApp(true, true)(http)
    }
  }

  private val routes: HttpRoutes[F] = Router(
    "/" -> openRoutes
  )

  private val middleware: HttpRoutes[F] => HttpRoutes[F] = {
    { http: HttpRoutes[F] =>
      AutoSlash(http)
    } andThen { http: HttpRoutes[F] =>
      CORS(http)
    } andThen { http: HttpRoutes[F] =>
      Timeout(10.seconds)(http)
    }
  }

  val httpApp: HttpApp[F] = loggers(middleware(routes).orNotFound)

}
