package io.scalac.config

import com.comcast.ip4s.{Host, Port}

object data {

  case class AppConfig(
      httpServerConfig: HttpServerConfig
  )

  case class HttpServerConfig(
      host: Host,
      port: Port
  )

}
