package io.scalac.services

import io.scalac.core.domain.graphql.GraphQlDto
import sangria.parser.QueryParser
import cats.data.EitherT
import cats.effect.Async
import cats.implicits._
import io.circe.Json
import io.scalac.db.repository.HeadlinesRepository
import io.scalac.sangria.SchemaDefinition
import sangria.execution._

import scala.concurrent.{ExecutionContext}
import scala.util.{Failure, Success}

trait GraphQLService[F[_]] {
  def executeQuery(dto: GraphQlDto): EitherT[F, String, Json]
}

object GraphQLService {

  implicit val ec: ExecutionContext = scala.concurrent.ExecutionContext.global

  import sangria.marshalling.circe.CirceInputUnmarshaller
  import sangria.marshalling.circe.CirceResultMarshaller

  def make[F[_]: Async](repo: HeadlinesRepository[F]) = new GraphQLService[F] {

    override def executeQuery(dto: GraphQlDto): EitherT[F, String, Json] = {
      QueryParser.parse(dto.query) match {
        case Failure(exception) => EitherT.leftT(exception.getMessage)
        case Success(queryAst) => {
          val value = Async[F].fromFuture {
            Executor.execute(SchemaDefinition.make[F](), queryAst,
              userContext = repo,
              operationName = dto.operation,
              variables = dto.variables,
              exceptionHandler = exceptionHandler,
            ).recover {
              case error: QueryAnalysisError => error.resolveError
              case error: ErrorWithResolver => error.resolveError
            }.pure[F]
          }
          EitherT.right(value)
        }
      }
    }

    lazy val exceptionHandler = ExceptionHandler {
      case (_, error @ TooComplexQueryError) => HandledException(error.getMessage)
      case (_, error @ MaxQueryDepthReachedError(_)) => HandledException(error.getMessage)
    }
    case object TooComplexQueryError extends Exception("Query is too expensive.")
  }
}
