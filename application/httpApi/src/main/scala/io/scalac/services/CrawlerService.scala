package io.scalac.services

import cats.{ Applicative, Monad }
import cats.data.EitherT
import cats.implicits._
import io.scalac.core.domain.crawler.CrawlDto
import io.scalac.core.domain.headline.{ HeadLineDto, HeadLinesDto }
import io.scalac.db.repository.HeadlinesRepository
import net.ruippeixotog.scalascraper.browser.{ Browser, JsoupBrowser }
import net.ruippeixotog.scalascraper.dsl.DSL._
import net.ruippeixotog.scalascraper.model.Document

import scala.util.Try

trait CrawlerService[F[_]] {
  def crawlAndSave(dto: CrawlDto): EitherT[F, String, HeadLinesDto]
}

object CrawlerService {

  private val headLineDefaultRetriever: Document => HeadLinesDto = doc => {
    val res = doc >> "section[class=\"story-wrapper\"]"
    val data = res.foldRight(List.empty[HeadLineDto])((story, list) => {
      val hlTitle = story.select("a.css-t059uy h3")
      if (hlTitle.size > 0) {
        val title = hlTitle.last.text
        val link  = story.select("a.css-t059uy").head.attrs("href")
        //println(s"title = $title")
        HeadLineDto(title, link) :: list
      } else list
    })
    HeadLinesDto(data)
  }

  def make[F[_]: Monad](repo: HeadlinesRepository[F]) = new CrawlerService[F] {

    override def crawlAndSave(dto: CrawlDto): EitherT[F, String, HeadLinesDto] = {
      val browser = JsoupBrowser()
      for {
        data <- getSiteData(browser, dto.url)
        _    <- repo.saveHeadlines(data)
      } yield data

    }

    private def getSiteData(
        browser: Browser,
        url: String,
        fn: Document => HeadLinesDto = headLineDefaultRetriever
    ): EitherT[F, String, HeadLinesDto] = EitherT.fromEither {
      Try {
        val doc = browser.get(url)
        fn(doc)
      }.toEither.leftMap(_.getMessage)
    }
  }

}
