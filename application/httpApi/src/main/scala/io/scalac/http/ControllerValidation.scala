package io.scalac.http

import cats.{Applicative, Monad}
import cats.data.EitherT
import cats.data.Validated._
import cats.implicits._
import io.scalac.util.ValidationUtil.{ValidationResult, notBlank}

trait ControllerValidation[F[_]] {
  def validateTwoArgs(arg1: String, arg2: String): EitherT[F, ValidationFailure, (String, String)]
}

case class ValidationFailure(msg: String) extends RuntimeException(msg) {
  def this(msg: String, throwable: Throwable) = {
    this(msg)
    initCause(throwable)
  }
}

object ControllerValidation {
  def make[F[_]: Monad](): ControllerValidation[F] =
    new ControllerValidation[F]() {
      override def validateTwoArgs(arg1: String, arg2: String): EitherT[F, ValidationFailure, (String, String)] =
        toValidationFailure2(notBlank("arg1", arg1), notBlank("arg2", arg2))
    }

  private def toValidationFailure2[F[_]: Applicative, A, B, Z](
      vr1: ValidationResult[A],
      vr2: ValidationResult[B]
  ): EitherT[F, ValidationFailure, (A, B)] =
    EitherT.fromEither[F] {
      (vr1, vr2)
        .mapN((v1, v2) => (v1, v2))
        .toEither
        .leftMap(nec => ValidationFailure(nec.combineAll))
    }
}
