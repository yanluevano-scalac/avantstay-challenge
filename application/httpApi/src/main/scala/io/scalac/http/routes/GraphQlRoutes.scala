package io.scalac.http.routes

import cats.MonadThrow
import cats.data.EitherT
import cats.implicits._
import io.circe.Decoder.Result
import io.circe.{Decoder, Encoder}
import io.scalac.core.domain.graphql.GraphQlDto
import io.scalac.services.GraphQLService
import org.http4s.circe.CirceEntityCodec.circeEntityEncoder
import org.http4s.circe.{JsonDecoder, toMessageSyntax}
import org.http4s.dsl.Http4sDsl
import org.http4s.server.Router
import org.http4s.{HttpRoutes, Request, Response}

class GraphQlRoutes[F[_]: JsonDecoder: MonadThrow](service: GraphQLService[F]) extends Http4sDsl[F] {

  private val prefixPath = "/graphql"
  private val httpRoutes: HttpRoutes[F] = HttpRoutes.of[F] {

    case req @ POST -> Root =>
      decode[GraphQlDto](req) { dto =>
        toResponse(service.executeQuery(dto))
      }
  }

  val routes: HttpRoutes[F] = Router(
    prefixPath -> httpRoutes
  )

  def toResponse[T: Encoder](e: EitherT[F, String, T]): F[Response[F]] =
    e.value.flatMap {
      case Left(exception) => InternalServerError(exception)
      case Right(value)    => Ok(value)
    }

  private def decode[A: Decoder](req: Request[F])(f: A => F[Response[F]]): F[Response[F]] =
    req.asJsonDecode[A].attempt.flatMap {
      case Left(ex)     => BadRequest(ex.getMessage)
      case Right(value) => f(value)
    }

}
