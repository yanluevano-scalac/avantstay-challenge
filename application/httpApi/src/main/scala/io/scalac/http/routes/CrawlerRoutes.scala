package io.scalac.http.routes

import cats.MonadThrow
import cats.data.EitherT
import io.scalac.services.CrawlerService
import org.http4s.{ HttpRoutes, Request, Response }
import org.http4s.dsl.Http4sDsl
import org.http4s.server.Router
import cats.implicits._
import io.circe.{ Decoder, Encoder }
import io.scalac.core.domain.crawler.CrawlDto
import org.http4s.circe.{ JsonDecoder, toMessageSyntax }
import io.circe.syntax._
import org.http4s.circe._

class CrawlerRoutes[F[_]: JsonDecoder: MonadThrow](service: CrawlerService[F]) extends Http4sDsl[F] {


  private val prefixPath = "/crawler"
  private val httpRoutes: HttpRoutes[F] = HttpRoutes.of[F] {

    case req @ POST -> Root / "crawl" =>
      decode[CrawlDto](req) { dto =>
        toResponse(service.crawlAndSave(dto))
      }
  }

  val routes: HttpRoutes[F] = Router(
    prefixPath -> httpRoutes
  )

  def toResponse[T: Encoder](e: EitherT[F, String, T]): F[Response[F]] =
    e.value.flatMap {
      case Left(value)  => InternalServerError(value)
      case Right(value) => Ok(value.asJson)
    }

  private def decode[A: Decoder](req: Request[F])(f: A => F[Response[F]]): F[Response[F]] =
    req.asJsonDecode[A].attempt.flatMap {
      case Left(ex)     => BadRequest(ex.getMessage)
      case Right(value) => f(value)
    }


}
