package io.scalac.core.domain

import derevo.circe.magnolia.{decoder, encoder}
import derevo.derive

object headline {

  @derive(decoder, encoder)
  case class HeadLineDto(title: String, link: String)

  @derive(decoder, encoder)
  case class HeadLinesDto(data: List[HeadLineDto])

}
