package io.scalac.core.domain

import derevo.circe.magnolia.{decoder, encoder}
import derevo.derive

object crawler {
  @derive(decoder, encoder)
  case class CrawlDto(url: String)
  /*object CrawlDto {
    implicit val jsonEncoder: Encoder[CrawlDto] = Encoder.forProduct1("url")(_.url)
    implicit val jsonDecoder: Decoder[CrawlDto] = Decoder.forProduct1("url")(CrawlDto.apply)
  }*/
}
