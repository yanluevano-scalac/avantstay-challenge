package io.scalac.core.domain

import derevo.circe.magnolia.encoder
import derevo.derive
import io.circe.Decoder.Result
import io.circe.{Decoder, Encoder, HCursor, Json}
import io.circe.literal._


object graphql {

  @derive(encoder)
  case class GraphQlDto(query: String, operation: Option[String], variables: Json)

  object GraphQlDto {
    implicit val jsonDecoder: Decoder[GraphQlDto] = new Decoder[GraphQlDto] {
      override def apply(c: HCursor): Result[GraphQlDto] = {
        val query = c.downField("query").as[String] match {
          case Left(value) => ""
          case Right(value) => value
        }
        val operation = c.downField("operationName").as[String] match {
          case Left(ex) => None
          case Right(value) => Some(value)
        }

        val variables = c.downField("variables").as[Json] match {
          case Left(ex) => Json.obj()
          case Right(value) => value
        }

        Right(GraphQlDto(query, operation, variables))
      }
    }

    /*implicit val decoder: Encoder[GraphQlDto] = new Encoder[GraphQlDto] {
      override def apply(a: GraphQlDto): Json =
        json"""{"query": ${a.query}}"""
    }*/

    private def toOpt[A](e: Result[A]) = e match {
      case Left(ex) => None
      case Right(value) => Some(value)
    }
  }

}
