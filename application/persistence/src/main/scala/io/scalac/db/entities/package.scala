package io.scalac.db

package entities {
 case class HeadLinesEntity(link: String, title: String)
}
