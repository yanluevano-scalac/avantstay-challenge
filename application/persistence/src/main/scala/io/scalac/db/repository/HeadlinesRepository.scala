package io.scalac.db.repository

import cats.data.EitherT
import cats.Applicative
import io.getquill.{ LowerCase, PostgresJdbcContext }
import io.scalac.core.domain.headline.HeadLinesDto
import io.scalac.db.entities.HeadLinesEntity
import cats.implicits._
import io.scalac.sangria.models.SimpleNews

import scala.util.Try

trait HeadlinesRepository[F[_]] {
  def saveHeadlines(headlines: HeadLinesDto): EitherT[F, String, Long]
  def getAllNewsSimple(): Try[List[SimpleNews]]
}

object HeadlinesRepository {
  def make[F[_]: Applicative](
      ctx: PostgresJdbcContext[LowerCase]
  ): HeadlinesRepository[F] = new HeadlinesRepository[F] {

    import ctx._
    val headlinesTable = quote(querySchema[HeadLinesEntity]("headlines"))

    def saveHeadlines(headlines: HeadLinesDto): EitherT[F, String, Long] = {
      val hls = headlines.data.map(h => HeadLinesEntity(h.link, h.title))
      EitherT.fromEither {
        Try {
          ctx
            .run(quote {
              liftQuery(hls).foreach(e => headlinesTable.insert(e))
            })
            .sum
        }.toEither.leftMap(_.getMessage)
      }
    }

    override def getAllNewsSimple(): Try[List[SimpleNews]] = Try {
      ctx.run(headlinesTable).map(h => SimpleNews(h.title, h.link))
    }
  }
}
