package io.scalac.sangria

import io.scalac.db.repository.HeadlinesRepository
import io.scalac.sangria.models.{ News, SimpleNews }
//import sangria.macros.derive._
import sangria.schema._

object SchemaDefinition {

  /*implicit lazy val SimpleNewsType = deriveObjectType[Unit, SimpleNews]()
  implicit lazy val QueryType = deriveInputObjectType[Query](
      ReplaceInputField("news", InputField("news", QueryType))
  )*/

  def make[F[_]](): Schema[HeadlinesRepository[F], Unit] = {
    val News: InterfaceType[HeadlinesRepository[F], News] =
      InterfaceType(
        "News",
        "Some News u will get",
        () =>
          fields[HeadlinesRepository[F], News](
            Field("title", StringType, Some("The title of the news."), resolve = _.value.title),
            Field("link", OptionType(StringType), Some("The link of the news."), resolve = _.value.link)
          )
      )

    val SimpleNews = ObjectType(
      "SimpleNews",
      "Some simple news",
      interfaces[HeadlinesRepository[F], SimpleNews](News),
      fields[HeadlinesRepository[F], SimpleNews](
        Field("title", StringType, Some("The title of the simple news."), resolve = _.value.title),
        Field("link", OptionType(StringType), Some("The link of the simple news."), resolve = _.value.link)
      )
    )

    val QueryType = ObjectType(
      "Query",
      fields[HeadlinesRepository[F], Unit](Field("allNews", ListType(SimpleNews), resolve = (ctx) => {
        ctx.ctx.getAllNewsSimple()
      }))
    )

    Schema(QueryType)
  }

}
