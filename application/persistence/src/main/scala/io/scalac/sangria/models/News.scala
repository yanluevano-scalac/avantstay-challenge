package io.scalac.sangria.models

trait News {
  def title: String
  def link: String
}

case class SimpleNews(title: String, link: String) extends News

case class Query(news: List[News])
