CREATE TABLE headlines (
  link VARCHAR PRIMARY KEY,
  title VARCHAR NOT NULL
);
INSERT INTO public.headlines (link,title) VALUES
	 ('https://www.nytimes.com/2021/07/27/health/covid-cdc-masks-vaccines-delta-variant.html','As Infections Rise, C.D.C. Urges Some Vaccinated Americans to Wear Masks Again'),
	 ('https://www.nytimes.com/live/2021/07/27/world/covid-delta-variant-vaccine/','Biden Weighs Requiring Federal Workers to Be Vaccinated or Face Testing'),
	 ('https://www.nytimes.com/2021/07/27/health/coronavirus-vaccination-hesitancy-delta.html','Frustrated by the prospect of a new surge of Covid cases, many Americans are blaming the unvaccinated.'),
	 ('https://www.nytimes.com/2021/07/27/health/cdc-masks-indoors-delta-variant.html','Here’s what we know about the C.D.C.’s new mask recommendations for vaccinated people.'),
	 ('https://www.nytimes.com/live/2021/07/27/us/politics-news/','‘I Have Kids,’ Capitol Officer Says He Pleaded as Rioters Beat Him'),
	 ('https://www.nytimes.com/2021/07/27/nyregion/capitol-riot-january-6.html','How a Respected N.Y.P.D. Officer Became the Accused Capitol Riot #EyeGouger'),
	 ('https://www.nytimes.com/2021/06/30/us/jan-6-capitol-attack-takeaways.html','The Times analyzed thousands of videos from the Jan. 6 attack to understand how it happened — and why. (June 30)'),
	 ('https://www.nytimes.com/live/2021/07/27/sports/gymnastics-olympics-results/','Biles Says She Left Gymnastics Final Over Mental State; Russia Wins Gold'),
	 ('https://www.nytimes.com/interactive/2021/07/27/sports/olympics/biles-vault-gymnastics.html','What Happened in Simone Biles’s Vault'),
	 ('https://www.nytimes.com/2021/07/27/world/asia/naomi-osaka-olympics-loss.html','Critics Pounce on Osaka After Loss, Denting Japan’s Claim to Diversity');
INSERT INTO public.headlines (link,title) VALUES
	 ('https://www.nytimes.com/interactive/2021/07/27/sports/olympics/olympics-2021-gymnastics-tennis-softball.html','See Tuesday’s action: Two of the biggest stars of the Games faltered while others celebrated.'),
	 ('https://www.nytimes.com/live/2021/07/26/sports/olympics-tokyo-medals-results/','Olympics Updates: Japan Beats U.S. for Softball Gold Medal'),
	 ('https://www.nytimes.com/2021/07/27/arts/design/artist-pension-trust.html','They Pooled Their Art to Create a Nest Egg. They Say It Was a Mistake.'),
	 ('https://www.nytimes.com/2021/07/27/science/lunar-rover-apollo-nasa.html','50 Years Ago, NASA Put a Car on the Moon'),
	 ('https://www.nytimes.com/2021/07/27/magazine/matt-damon.html','Matt Damon’s Disappearing Acts'),
	 ('https://www.nytimes.com/2021/07/04/opinion/simone-biles-gymnastics.html','Simone Biles Doesn’t Need a Gold Medal to Win'),
	 ('https://www.nytimes.com/2021/07/27/opinion/pulsars-jocelyn-bell-burnell-astronomy.html','She Changed Astronomy Forever. He Won the Nobel Prize for It.'),
	 ('https://www.nytimes.com/2021/07/27/opinion/pope-francis-catholic-church.html','The Ungovernable Catholic Church'),
	 ('https://www.nytimes.com/2021/07/27/opinion/us-founding-fathers-constitution.html','George Washington Feared for America and Other Truths About the Founders We’ve Frozen in Time'),
	 ('https://www.nytimes.com/2021/07/27/opinion/adam-kinzinger-jan-6-committee.html','I’m on the Jan. 6 Committee. Here Are the Questions I Want Answered.');
INSERT INTO public.headlines (link,title) VALUES
	 ('https://www.nytimes.com/2021/07/27/opinion/israel-ben-jerrys-cybersecurity.html','Israel Wants to Have Its Ice Cream and Cybersecurity Too'),
	 ('https://www.nytimes.com/2021/07/27/opinion/ezra-klein-podcast-ross-douthat.html','Ross Douthat Has Been ‘Radicalized a Little Bit, Too’'),
	 ('https://www.nytimes.com/2021/07/27/opinion/us-china-trade-tariffs.html','America Shouldn’t Compete Against China With One Arm Tied Behind Its Back'),
	 ('https://www.nytimes.com/2021/07/26/opinion/cornell-birdsong-id-app.html','This ‘Shazam’ for Birds Could Help Save Them'),
	 ('https://www.nytimes.com/2021/07/26/opinion/child-tax-credit-family-values.html','Return of the Family Values Zombie'),
	 ('https://www.nytimes.com/2021/07/26/opinion/jan-6-commission-pelosi.html','Nancy Pelosi Is Working in a World Where the Other Side Won’t Play Fair'),
	 ('https://www.nytimes.com/2021/07/26/opinion/homelessness-california.html','It’s Hard to Have Faith in a State That Can’t Even House Its People'),
	 ('https://www.nytimes.com/2021/07/26/opinion/covid-return-to-office-work-houston.html','We’re Kidding Ourselves That Workers Perform Well From Home'),
	 ('https://www.nytimes.com/2021/07/26/opinion/biden-george-w-bush-covid.html','The World Needs a Heavy Hitter on the Pandemic. Bush Has Done It Before.'),
	 ('https://www.nytimes.com/interactive/2021/07/26/arts/design/mom-deepfake-akkapeddi.html','Can Fake Images Show Us Something Real?');
INSERT INTO public.headlines (link,title) VALUES
	 ('https://www.nytimes.com/2021/07/27/us/man-runs-eastern-seaboard-hamster-wheel.html','He Tried to Walk on Water From Florida to N.Y. It Didn’t Go Well.'),
	 ('https://www.nytimes.com/article/what-is-critical-race-theory.html','Critical Race Theory: A Brief History');