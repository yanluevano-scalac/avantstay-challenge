package io.scalac

import cats.effect.IO
import cats.effect.unsafe.implicits.global
import com.opentable.db.postgres.embedded.EmbeddedPostgres
import org.scalatest._
import flatspec._
import io.circe.Decoder.Result
import io.circe.{Decoder, HCursor, Json}
import io.scalac.core.domain.graphql.GraphQlDto
import io.scalac.db.DataSource
import io.scalac.db.repository.HeadlinesRepository
import io.scalac.sangria.models.SimpleNews
import io.scalac.services.GraphQLService
import matchers._
import org.flywaydb.core.Flyway
import io.circe.generic.auto._

class SchemaSpec extends AnyFlatSpec with should.Matchers with BeforeAndAfter {

  var repository: HeadlinesRepository[IO] = _
  var db: EmbeddedPostgres = _
  var graphQLService: GraphQLService[IO] = _

  before {
    db = EmbeddedPostgres.builder().setPort(5433).start()
    val postgresCtx = DataSource.make(5433)
    doFlywayMigration(postgresCtx.dataSource)
    repository = HeadlinesRepository.make[IO](postgresCtx)
    graphQLService = GraphQLService.make[IO](repository)
  }

  after {
    db.close()
  }

  "HeadLines table" should "have 32 entries" in {
    val data = repository.getAllNewsSimple()
    data.isSuccess should be (true)
    data.get.size should be (32)
  }

  "Schema" should "return all news" in {
    val query = """query {
                  |  allNews {
                  |    title
                  |    link
                  |  }
                  |}""".stripMargin
    val dec = Decoder[List[SimpleNews]].prepare(
      _.downField("data").downField("allNews")
    )
    val dto = GraphQlDto(query, None, Json.obj())
    val ioResult = graphQLService.executeQuery(dto)
    val result = ioResult.value.unsafeRunSync()
    result.isRight should be (true)

    result match {
      case Left(ex) =>
      case Right(value) => {
        value.as[List[SimpleNews]](dec) match {
          case Left(ex) =>
          case Right(value) => {
            value.size should be (32)
          }
        }
      }
    }

  }

  def doFlywayMigration(dataSource: javax.sql.DataSource): Unit =
    Flyway.configure()
    .dataSource(dataSource)
    .load()
    .migrate()

}
