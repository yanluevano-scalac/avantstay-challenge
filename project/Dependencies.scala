import sbt._

object Dependencies {

  lazy val testModuleDependencies: Seq[ModuleID] = Seq(
    Libraries.cats,
    Libraries.circeGeneric,
    Libraries.circeParser,
    Libraries.derevoCore,
    Libraries.derevoCats,
    Libraries.derevoCirce,
    Libraries.circeLiteral,
    Libraries.sangria,
    Libraries.scalaTest,
    Libraries.openTable,
    Libraries.junit,
    Libraries.scalaTestPlus,
    Libraries.flyway
  )

  lazy val coreModuleDependencies: Seq[ModuleID] = Seq(
    Libraries.cats,
    Libraries.log4cats,
    Libraries.logback,
    Libraries.circeGeneric,
    Libraries.circeParser,
    Libraries.derevoCore,
    Libraries.derevoCats,
    Libraries.derevoCirce,
    Libraries.circeLiteral,
    Libraries.sangria
  )
  lazy val httpModuleDependencies: Seq[ModuleID] = Seq(
    Libraries.cats,
    Libraries.log4cats,
    Libraries.logback,
    Libraries.http4sDsl,
    Libraries.http4sServer,
    Libraries.http4sClient,
    Libraries.http4sCirce,
    Libraries.circeGeneric,
    Libraries.circeParser,
    Libraries.derevoCore,
    Libraries.derevoCats,
    Libraries.derevoCirce,
    Libraries.sttpCore,
    Libraries.scalaScrapper,
    Libraries.sangria,
    Libraries.sangriaCirce
  )
  lazy val persistenceModuleDependencies: Seq[ModuleID] = Seq(
    Libraries.cats,
    Libraries.log4cats,
    Libraries.logback,
    Libraries.openTable,
    Libraries.hikari,
    Libraries.quill,
    Libraries.postgres
  )

  private object V {
    val cats          = "2.6.1"
    val http4s        = "1.0.0-M23"
    val log4cats      = "2.1.1"
    val logback       = "1.2.3"
    val circe         = "0.14.1"
    val derevo        = "0.12.6"
    val sttpCore      = "3.0.0-RC3"
    val sangria       = "2.1.3"
    val sangriaCirce  = "1.3.1"
    val scalaScrapper = "2.2.1"
    val openTable     = "0.13.4"
    val hikari        = "4.0.0"
    val quill         = "3.8.0"
    val postgres      = "42.2.8"
    val scalaTest     = "3.2.9"
    val junit         = "4.10"

  }

  private object Libraries {
    val cats          = "org.typelevel" %% "cats-core" % V.cats
    val log4cats      = "org.typelevel" %% "log4cats-slf4j" % V.log4cats
    val http4sDsl     = http4s("dsl")
    val http4sServer  = http4s("ember-server")
    val http4sClient  = http4s("ember-client")
    val http4sCirce   = http4s("circe")
    val circeCore     = circe("core")
    val circeLiteral  = circe("literal")
    val circeGeneric  = circe("generic")
    val circeParser   = circe("parser")
    val logback       = "ch.qos.logback" % "logback-classic" % V.logback
    val derevoCore    = derevo("core")
    val derevoCats    = derevo("cats")
    val derevoCirce   = derevo("circe-magnolia")
    val sttpCore      = "com.softwaremill.sttp.client" %% "core" % V.sttpCore
    val sangria       = "org.sangria-graphql" %% "sangria" % V.sangria
    val sangriaCirce  = "org.sangria-graphql" %% "sangria-circe" % V.sangriaCirce
    val scalaScrapper = "net.ruippeixotog" %% "scala-scraper" % V.scalaScrapper
    val hikari        = "com.zaxxer" % "HikariCP" % V.hikari
    val quill         = "io.getquill" %% "quill-jdbc" % V.quill withSources ()
    val postgres      = "org.postgresql" % "postgresql" % V.postgres
    val openTable     = "com.opentable.components" % "otj-pg-embedded" % V.openTable % Test
    val scalaTest     = "org.scalatest" %% "scalatest" % V.scalaTest % Test
    val junit         = "junit" % "junit" % V.junit % Test
    val scalaTestPlus = "org.scalatestplus" %% "scalatestplus-junit" % "1.0.0-M2" % Test
    val flyway = "org.flywaydb" % "flyway-core" % "7.11.4" % Test

    def derevo(artifact: String): ModuleID = "tf.tofu"    %% s"derevo-$artifact" % V.derevo
    def http4s(artifact: String): ModuleID = "org.http4s" %% s"http4s-$artifact" % V.http4s
    def circe(artifact: String): ModuleID  = "io.circe"   %% s"circe-$artifact"  % V.circe

  }

}
