import Dependencies._

ThisBuild / scalaVersion := "2.13.5"
ThisBuild / version := "1.0.0"
ThisBuild / organization := "io.scalac"
ThisBuild / organizationName := "scalac"

lazy val root = (project in file("."))
  .settings(
    name := "avantstay-graphql",
  )
  .aggregate(core, httpApi, persistence, tests)

lazy val tests = (project in file("application/tests"))
  .configs(IntegrationTest)
  .settings(
    name := "avantstay-graphql-tests",
    scalacOptions ++= List("-Ymacro-annotations"),
    libraryDependencies ++= testModuleDependencies
  )
  .dependsOn(core, httpApi, persistence)

lazy val core = (project in file(".") / "application" / "core")
  .settings(
    name := "avantstay-graphql-core",
    scalacOptions ++= List("-Ymacro-annotations"),
    libraryDependencies ++= coreModuleDependencies
  )

lazy val httpApi = (project in file(".") / "application" / "httpApi")
  .settings(
    name := "avantstay-graphql-http",
    scalacOptions ++= List("-Ymacro-annotations"),//, "-Ymacro-debug-verbose"),
    libraryDependencies ++= httpModuleDependencies
  )
  .dependsOn(core, persistence)

lazy val persistence = (project in file(".") / "application" / "persistence")
  .settings(
    name := "avantstay-graphql-persistence",
    libraryDependencies ++= persistenceModuleDependencies
  )
  .dependsOn(core)
